# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
name = "Harpylen-Gelkalis War"
war_goal = {
	type = superiority_monster
	casus_belli = cb_monster_vs_civ
}

#
1435.1.1 = {
	add_attacker = F27
	add_defender = F26
}
#
1444.5.1 = {
	rem_attacker = F27
	rem_defender = F26
}