# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# No previous file for Naisha
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B21
hre = no

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = naval_supplies

native_size = 33
native_ferocity = 9
native_hostileness = 8