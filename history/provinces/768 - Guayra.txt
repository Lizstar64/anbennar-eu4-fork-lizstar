# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# No previous file for Guayra
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B22
hre = no

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = unknown

native_size = 33
native_ferocity = 4
native_hostileness = 5