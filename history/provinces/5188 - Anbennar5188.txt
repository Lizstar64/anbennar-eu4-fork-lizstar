# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
culture = plain_centaur
religion = ik_magthaal
hre = no

base_tax = 1
base_production = 1
base_manpower = 1
native_size = 3
native_ferocity = 2
native_hostileness = 6

trade_goods = unknown

capital = ""