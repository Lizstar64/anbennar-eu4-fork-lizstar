country_decisions = {

	#-------# Bulwar Under Threat Incident #-----#
	
	nsc_proclaim_pledge_fulfilled = {
		major = yes
		potential = {
			has_country_flag = nsc_pledge
		}
		
		provinces_to_highlight = {
			OR = {
				AND = { ROOT = { tag = F21 } province_group = nsc_incident_target_birsartanses }
				AND = { ROOT = { tag = F25 } province_group = nsc_incident_target_sareyand }
				AND = { ROOT = { tag = F34 } province_group = nsc_incident_target_azka_evran }
				AND = { ROOT = { tag = F37 } province_group = nsc_incident_target_irrliam }
				AND = { ROOT = { tag = F42 } province_group = nsc_incident_target_varamhar }
			}
		}
		
		allow = {
			custom_trigger_tooltip = {
				tooltip = nsc_pledge_score_tt
				check_variable = { which = ScoreVar value = 66 }
			}
			# NOT = { has_country_flag = nsc_pledge_menu }
			# OR = {
				# ai = no
				# NOT = { has_country_flag = nsc_ai_pledge_menu }
				# had_country_flag = { flag = nsc_ai_pledge_menu days = 1095 } #prevents the ai from spamming the decision
			# }
		}
	
		effect = {
			set_country_flag = nsc_pledge_menu
			country_event = { id = new_sun_cult.77 }
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	nsc_reform_admin = {
		potential = {
			has_country_flag = nsc_pledge
			NOT = { has_country_flag = nsc_reforming_admin }
			NOT = { has_country_flag = nsc_admin_over }
			NOT = { has_country_flag = nsc_reining_in_populace }
			NOT = { has_country_flag = nsc_appeasing_vassals }
			NOT = { has_country_flag = nsc_reforming_army }
		}
		
		allow = { }
	
		effect = {
			set_country_flag = nsc_reforming_admin
			country_event = { id = new_sun_cult.52 }
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	nsc_rein_in_populace = {
		potential = {
			has_country_flag = nsc_pledge
			NOT = { has_country_flag = nsc_reforming_admin }
			NOT = { has_country_flag = nsc_reining_in_populace }
			NOT = { has_country_flag = nsc_populace_over }
			NOT = { has_country_flag = nsc_appeasing_vassals }
			NOT = { has_country_flag = nsc_reforming_army }
		}
		
		allow = { }
	
		effect = {
			set_country_flag = nsc_reining_in_populace
			country_event = { id = new_sun_cult.58 }
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	nsc_appease_vassals = {
		potential = {
			has_country_flag = nsc_pledge
			religion = bulwari_sun_cult
		}
		
		allow = {
			OR = {
				ai = no
				NOT = { has_country_flag = nsc_ai_took_vassal_decision }
				had_country_flag = { flag = nsc_ai_took_vassal_decision days = 1095 } #prevents the ai from spamming the decision
			}
		}
	
		effect = {
			set_country_flag = nsc_appeasing_vassals
			country_event = { id = new_sun_cult.63 }
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	nsc_reform_the_army = {
		potential = {
			has_country_flag = nsc_pledge
			religion = bulwari_sun_cult
			NOT = { has_country_flag = nsc_reforming_admin }
			NOT = { has_country_flag = nsc_reining_in_populace }
			NOT = { has_country_flag = nsc_appeasing_vassals }
			NOT = { has_country_flag = nsc_reforming_army }
			NOT = { has_country_flag = nsc_army_over }
		}
		
		allow = { }
	
		effect = {
			set_country_flag = nsc_reforming_army
			country_event = { id = new_sun_cult.68 }
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	#Ask for more autonomy( vassal)
	nsc_demand_more_autonomy = {
		potential = {
			ai = no
			religion = bulwari_sun_cult
			overlord = { is_incident_active = incident_bulwar_under_threat }
			OR = {
				is_subject_of_type = vassal
				is_subject_of_type = march
			}
			OR = {
				NOT = { has_country_flag = nsc_asking_for_autonomy }
				had_country_flag = { flag = nsc_asking_for_autonomy days = 730 }
			}
		}
		
		allow = {
			custom_trigger_tooltip = { 
				tooltip = nsc_isolationism_level_is_3_tt
				isolationism = 3
			}
		}
	
		effect = {
			country_event = { id = new_sun_cult.73 }
			set_country_flag = nsc_asking_for_autonomy
		}
		
		ai_will_do = {
			factor = 0
		}
	}
	
	#-----------# Samartal Incident #------------#
	
	nsc_investigation_progress_decision = {
		potential = {
			has_country_flag = nsc_investigation_enabled
		}
		
		allow = {
			custom_trigger_tooltip = {
				tooltip = nsc_investigation_variable_tt
				always = yes
			}
		}
	
		effect = {
			
		}
		
		ai_will_do = {
			factor = 0
		}
	}
	
	#---------------# Debuging #-----------------#
	
	# add_isolationism = {	
		# potential = {
			# religion = bulwari_sun_cult
		# }
		
		# allow = {
			
		# }
	
		# effect = {
			# increase_nsc_isolation_level = yes
		# }
		# ai_will_do = {
			# factor = 0
		# }
	# }
	
	# sub_isolationism = {	
		# potential = {
			# religion = bulwari_sun_cult
		# }
		
		# allow = {
			
		# }
	
		# effect = {
			# decrease_nsc_isolation_level = yes
		# }
		# ai_will_do = {
			# factor = 0
		# }
	# }
}
