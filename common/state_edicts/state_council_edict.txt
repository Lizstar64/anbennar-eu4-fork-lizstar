# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
state_council_edict = {
	potential = {
		has_country_flag = federation_state_council
	}
	
	allow = {
		always = yes
	}
	
	modifier = {
		local_governing_cost = -0.33
	}
	
	color = { 70 125 230 }
	
	
	ai_will_do = {
		factor = 200
	}
}
