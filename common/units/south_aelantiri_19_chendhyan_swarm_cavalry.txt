# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# Ram Raiders

type = cavalry
unit_type = tech_south_aelantiri

maneuver = 2
offensive_morale = 3
defensive_morale = 3
offensive_fire = 1
defensive_fire = 1
offensive_shock = 3
defensive_shock = 3
