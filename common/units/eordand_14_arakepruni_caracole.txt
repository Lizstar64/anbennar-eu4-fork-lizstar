# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# Arakepruni Caracole

unit_type = tech_eordand
type = cavalry

maneuver = 2
offensive_morale = 2
defensive_morale = 2
offensive_fire = 1
defensive_fire = 1
offensive_shock = 3
defensive_shock = 1
