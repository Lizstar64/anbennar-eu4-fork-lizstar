# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# Schwarze Reiter | its knights

type = cavalry
unit_type = tech_salahadesi

maneuver = 2
offensive_morale = 2
defensive_morale = 2
offensive_fire = 1
defensive_fire = 0
offensive_shock = 2
defensive_shock = 2
