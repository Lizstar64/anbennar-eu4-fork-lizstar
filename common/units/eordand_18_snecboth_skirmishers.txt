# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# Snecboth Skirmishers

unit_type = tech_eordand
type = cavalry

maneuver = 2
offensive_morale = 3
defensive_morale = 3
offensive_fire = 2
defensive_fire = 2
offensive_shock = 3
defensive_shock = 2
