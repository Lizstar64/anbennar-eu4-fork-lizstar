# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
#
# Pips: 21

type = cavalry
unit_type = tech_ynnic

maneuver = 2
offensive_morale = 5
defensive_morale = 4
offensive_fire = 0
defensive_fire = 2
offensive_shock = 6
defensive_shock = 4
